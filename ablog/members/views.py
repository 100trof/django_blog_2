from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from requests import request

from .forms import SignUpForm
from django.contrib import messages


# class UserRegisterView(generic.CreateView):
#     form_class = SignUpForm
#     template_name = 'registration/register.html'
#     success_url = reverse_lazy('login')
#     messages.success(XXX, 'Вы успешно зарегистрировались!')

def UserRegisterView(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Вы успешно зарегистрировались! :)')
            return redirect('login')
        else:
            messages.error(request, 'Ошибка при регистрации :(')
    else:
        form = SignUpForm()
    return render(request, 'registration/register.html', {'form': form})
