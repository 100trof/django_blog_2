from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import date, datetime

class Category(models.Model):
    name = models.CharField('Название категории', max_length=255)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('home')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Post(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    title_tag = models.CharField('Тег', max_length=255, default='')
    category = models.CharField('Категория',max_length=255, default='Общее')
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    body = models.TextField('Текст статьи',)
    post_date = models.DateField(auto_now_add=True)
    likes = models.ManyToManyField(User, related_name='blog_posts')


    def total_likes(self):
        return self.likes.count()

    def __str__(self):
        return self.title + ' | ' + str(self.author)

    def get_absolute_url(self):
        return reverse('home')

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'
