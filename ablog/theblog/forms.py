from django import forms
from .models import Post, Category

choices = Category.objects.all().values_list('name', 'name')
choice_list = []

for item in choices:
    choice_list.append(item)

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'title_tag', 'author', 'category', 'body')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Название статьи'}),
            'title_tag': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Теги'}),
            'author': forms.TextInput(attrs={'class': 'form-control', 'value': '', 'id': 'user_author', 'type': 'hidden'}),
            # 'author': forms.Select(attrs={'class': 'form-control', 'placeholder': 'Автор'}),
            'category': forms.Select(choices=choice_list, attrs={'class': 'form-control', 'placeholder': 'Категория'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Текст статьи'}),
         }

class EditForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'title_tag', 'body')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Название статьи'}),
            'title_tag': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Теги'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Текст статьи'}),
        }